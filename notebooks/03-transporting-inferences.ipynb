{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "# Transporting Inferences\n",
    "\n",
    "In this notebook, we cover transporting inferences between different domains.\n",
    "\n",
    "We consider a scenario where we perform an observational study measuring participant ratings of decision support systems in China, and we also have information about receptiveness to robot-issued decision support in the United States. Using this data, we want to estimate participant ratings of decision support in the United States.\n",
    "\n",
    "* Treatment (A): Decision support system type (0: computer-issued; 1: robot-issued)\n",
    "* Confounder (C): Receptiveness to robot-issued decision support, on a scale of 0-4\n",
    "* Outcome (Y): Rating of decision support system, on a scale of 0-4"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "from causal_hri import data\n",
    "from causal_hri import estimators"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We generate the source data from the Chinese domain. We collect data $(A_i, C_i, Y_i)_{i=1}^N$ corresponding to the treatment, confounder, and outcome, respectively, from an observational study."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "N = 500\n",
    "df_c = data.generate_transportability_dataset(N=N, domain=\"China\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "decision_system    0.868\n",
       "receptiveness      1.004\n",
       "rating             3.416\n",
       "dtype: float64"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df_c.mean()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We generate the full target data from the American domain, but in the transportable estimator, we will only use the information about the confounder (receptiveness to robotic recommendation), as if we had conducted a survey collecting only that information. This is represented by $(C_i)_{i=1}^N$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_u = data.generate_transportability_dataset(N=N, domain=\"US\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "decision_system    0.930\n",
       "receptiveness      1.944\n",
       "rating             3.740\n",
       "dtype: float64"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df_u.mean()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The population causal effect of the decision support system on participant rating in the United States is \n",
    "$$p^*(Y(a)) = \\sum_C p(Y | A, C) p^*(C) $$\n",
    "where distributions $p( \\cdot)$ are drawn from China, and $p^*(\\cdot)$ are drawn from the United States.\n",
    "\n",
    "We can estimate the causal effect by\n",
    "$$E[Y(a)] = \\frac{1}{N}\\sum_{i=1}^N \\hat{E}[Y | A=a, C=C'_i]$$ \n",
    "\n",
    "where $\\hat{E}$ is formed by training on data $(A_i, C_i, Y_i)$ from China but is then evaluated at a fixed value of $A$ and confounder data $C'_i$ from the United States. We will call this the _transported estimate_.\n",
    "\n",
    "We can compare this estimate to an estimate computed on the full target data $(A'_i, C'_i, Y'_i)$ which represents the full data collected from an observational study conducted in the United States. In this estimate, we simply adjust for the confounding using the g-formula. We will call this the _target estimate_."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "n_bootstraps = 100\n",
    "\n",
    "transported = []\n",
    "target = []\n",
    "for i in range(n_bootstraps):\n",
    "    df_u_sample = df_u.sample(frac=1, replace=True)\n",
    "    df_c_sample = df_c.sample(frac=1, replace=True)\n",
    "    est_transport = estimators.estimate_g_formula_transportable(data_source=df_c_sample, data_target=df_u_sample[[\"receptiveness\"]], outcome=\"rating\",\n",
    "                                           treatment=\"decision_system\", treatment_value=1, confounders=[\"receptiveness\"]) - estimators.estimate_g_formula_transportable(data_source=df_c_sample, data_target=df_u_sample[[\"receptiveness\"]], outcome=\"rating\",\n",
    "                                           treatment=\"decision_system\", treatment_value=0, confounders=[\"receptiveness\"])\n",
    "    transported.append(est_transport)\n",
    "    est_target = estimators.estimate_g_formula(data=df_u_sample, outcome=\"rating\",\n",
    "                                           treatment=\"decision_system\", treatment_value=1, confounders=[\"receptiveness\"]) - estimators.estimate_g_formula(data=df_u_sample, outcome=\"rating\",\n",
    "                                           treatment=\"decision_system\", treatment_value=0, confounders=[\"receptiveness\"])\n",
    "    target.append(est_target)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this simulation, the true average causal effect is about 0.8. We see that both estimators (transported and target) recover this causal effect well. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<matplotlib.legend.Legend at 0x13af7ab20>"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAXcAAAD7CAYAAACRxdTpAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjUuMSwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy/YYfK9AAAACXBIWXMAAAsTAAALEwEAmpwYAAAVOElEQVR4nO3dfZRU9Z3n8fd30bFREB0ghgNhcJxEwNg0pkGyhEyrWc2YHI0aE3ETJZBFzUqYnDEOOBo5m+yJJzpMzppkEgyk3TkSmR0Th5x1d0iMhuBDQiONtqBBWB+acQAxLXiACa2//aOKtuWpn6qrih/v1zl9+ta9t+79dNH94datW7+KlBKSpLz8h0oHkCSVnuUuSRmy3CUpQ5a7JGXIcpekDFnukpShLss9It4XEY9ExPqIeDYi5hbnL4iILRHRXPy6uP/jSpK6I7q6zj0iRgAjUkpPRcRgYA3wKeAzwJsppbv6PaUkqUeO62qFlNKrwKvF6V0RsQEY2ZudDRs2LI0ZM6Y3d5Xg+ecL3888s7I5pDJbs2bNayml4T25T5fl3llEjAEmAr8BpgI3RsQ1QBPwVyml3x/p/mPGjKGpqaknu5Te0dBQ+P7oo5VMIZVdRLzU0/t0+wXViBgEPAD8ZUppJ/D3wBlAHYUj+789zP1mR0RTRDRt3769p/kkSb3QrXKPiOMpFPt9KaWfAKSUtqaU3kopvQ3cA0w+1H1TSotSSvUppfrhw3v0rEKS1EvduVomgMXAhpTSwk7zR3Ra7TKgpfTxJEm90Z1z7lOBzwPPRERzcd4twPSIqAMS8CJwXT/kk1QB+/bto7W1lb1791Y6yjGlpqaGUaNGcfzxx/d5W925WmYVEIdY9FCf9y6pKrW2tjJ48GDGjBlD4cm7+ltKiR07dtDa2srpp5/e5+35DlVJB9m7dy9Dhw612MsoIhg6dGjJni1Z7pIOyWIvv1I+5pa7pKqzY8cO6urqqKur473vfS8jR47suP2HP/yh0vHepbm5mYce6vlZ6oaGhn5930+P3sQk6dg0q3F1Sbe3eMakIy4fOnQozc3NACxYsIBBgwZx0003dSxvb2/nuOMqX1/t7e00NzfT1NTExRdX1/BalX90VHVK/YfcE1390evYNWPGDGpqali7di1Tp07lqquuYu7cuezdu5eBAwfyox/9iDPPPJPGxkaWL1/O7t272bRpE5dddhnf+ta3eOutt5g1axZNTU1EBDNnzuQrX/kKDQ0NTJgwgV/96le0t7ezZMkSJk+ezOuvv87MmTPZvHkzJ554IosWLaK2tpYFCxawadMmNm/ezOjRo3nsscfYs2cPq1atYv78+Xzyk59kzpw5tLS0sG/fPhYsWMCll17Knj17+MIXvsC6desYO3Yse/bs6dfHy3KXdNRobW3l8ccfZ8CAAezcuZNf//rXHHfccfziF7/glltu4YEHHgAKp0rWrl3LCSecwJlnnsmcOXPYtm0bW7ZsoaWl8Jactra2ju3u3r2b5uZmVq5cycyZM2lpaeH2229n4sSJPPjgg/zyl7/kmmuu6Xg2sX79elatWsXAgQNpbGykqamJ73znOwDccsstnH/++SxZsoS2tjYmT57Mxz72MX7wgx9w4oknsmHDBp5++mnOOeecfn2sLHdJR40rr7ySAQMGAPDGG29w7bXXsnHjRiKCffv2dax3wQUXMGTIEADGjx/PSy+9xFlnncXmzZuZM2cOn/jEJ7jwwgs71p8+fToAH/3oR9m5cydtbW2sWrWq4z+L888/nx07drBz504ALrnkEgYOHHjIjCtWrGD58uXcdVdhwNy9e/fy8ssvs3LlSr785S8DUFtbS21tbSkfmoP4gqqko8ZJJ53UMX3bbbdx3nnn0dLSws9+9rN3XUJ4wgkndEwPGDCA9vZ2Tj31VNatW0dDQwPf//73+eIXv9ixzoFXqXR11UrnHAdKKfHAAw/Q3NxMc3MzL7/8MuPGjev2z1gqlruko9Ibb7zByJGF0ccbGxu7XP+1117j7bff5oorruAb3/gGTz31VMeyZcuWAbBq1SqGDBnCkCFDmDZtGvfddx8Ajz76KMOGDePkk08+aLuDBw9m165dHbcvuugi7r77bvZ/VsbatWuBwrOCpUuXAtDS0sLTTz/di5+6+yx3SUelm2++mfnz5zNx4kTa29u7XH/Lli00NDRQV1fH5z73Ob75zW92LKupqWHixIlcf/31LF68GChcpbNmzRpqa2uZN28e99577yG3e95557F+/Xrq6upYtmwZt912G/v27aO2tpazzjqL2267DYAbbriBN998k3HjxvG1r32ND33oQyV4FA6vy09iKqX6+vrkeO7Vr2qvlnE897LZsGFDRU4lVEJDQwN33XUX9fX1lY4CHPqxj4g1KaUeBfTIXZIy5NUyOjos/SxsW//OdDlcvaw8+1FFPZrpM0GP3CUpQ5a7JGXIcpekDFnukpQhy11S1SnnkL9tbW1873vfK+k2q4FXy0jqWqmvUOriSqSuhvw9nN4MBby/3L/0pS/16H7VziN3SUeFe+65h0mTJjFhwgSuuOIKdu/eDRSGAr7++us599xzufnmm9m0aRNTpkzh7LPP5tZbb2XQoEEd27jzzjuZNGkStbW13H777QDMmzePTZs2UVdXx1e/+tWK/Gz9wXKXdFS4/PLLWb16NevWrWPcuHEdwwTAO0MBL1y4kLlz5zJ37lyeeeYZRo0a1bHOihUr2LhxI7/97W9pbm5mzZo1rFy5kjvuuIMzzjiD5uZm7rzzzkr8aP3Ccpd0VGhpaWHatGmcffbZ3HfffTz77LMdyzoPBfzEE09w5ZVXAnD11Vd3rLNixQpWrFjBxIkTOeecc3juuefYuHFjeX+IMvKcu6SjwowZM3jwwQeZMGECjY2N73pn6ZGG4N0vpcT8+fO57rrr3jX/xRdfLHHS6uCRu6Sjwq5duxgxYgT79u3rGIr3UKZMmdLxIRv3339/x/yLLrqIJUuW8OabbwKFUSK3bdt20JC9ubDcJR0Vvv71r3PuuecydepUxo4de9j1vv3tb7Nw4UJqa2t54YUXOj6R6cILL+Tqq6/mwx/+MGeffTaf/vSn2bVrF0OHDmXq1Kl88IMfzOoFVU/LSOpaBQdRW7BgQcf0DTfccNDyAz+oY+TIkTz55JNEBPfffz/PP/98x7L9L7YeaP+HaOTEcpeUlTVr1nDjjTeSUuKUU05hyZIllY5UEZa7pKxMmzaNdevWVTpGxXnOXZIyZLlLOqRyfgSnCkr5mFvukg5SU1PDjh07LPgySimxY8cOampqSrI9z7lLOsioUaNobW1l+/btlY5yTKmpqXnXkAl9YblLOsjxxx/P6aefXukY6gNPy0hShros94h4X0Q8EhHrI+LZiJhbnP/HEfHziNhY/H5q/8eVJHVHd47c24G/SimNB6YA/zUixgPzgIdTSu8HHi7eliRVgS7LPaX0akrpqeL0LmADMBK4FLi3uNq9wKf6KaMkqYd6dM49IsYAE4HfAKellF4tLvo34LTSRpMk9Va3yz0iBgEPAH+ZUtrZeVkqXAx7yAtiI2J2RDRFRJOXVUlSeXSr3CPieArFfl9K6SfF2VsjYkRx+Qhg26Hum1JalFKqTynVDx8+vBSZJUld6M7VMgEsBjaklBZ2WrQcuLY4fS3wz6WPJ0nqje68iWkq8HngmYhoLs67BbgD+MeImAW8BHymXxJKknqsy3JPKa0C4jCLLyhtHElSKfgOVUnKkOUuSRmy3CUpQ5a7JGXIIX8loPmVtoPm3d24uiz7XjxjUln2o2OLR+6SlCHLXZIyZLlLUoYsd0nKkOUuSRmy3CUpQ5a7JGXI69yPZks/2y+bnbO17YjL7z7tG/2yX0ml45G7JGXIcpekDFnukpQhy12SMmS5S1KGLHdJypDlLkkZstwlKUOWuyRlyHKXpAxZ7pKUIctdkjJkuUtShix3ScqQ5S5JGbLcJSlDlrskZchPYlJVmdW4+pDz52xt48/2tgPwwittZUwkHZ08cpekDFnukpQhy12SMtRluUfEkojYFhEtneYtiIgtEdFc/Lq4f2NKknqiO0fujcDHDzH/71JKdcWvh0obS5LUF12We0ppJfB6GbJIkkqkL+fcb4yIp4unbU4tWSJJUp/1ttz/HjgDqANeBf72cCtGxOyIaIqIpu3bt/dyd5KknuhVuaeUtqaU3kopvQ3cA0w+wrqLUkr1KaX64cOH9zanJKkHelXuETGi083LgJbDrStJKr8uhx+IiB8DDcCwiGgFbgcaIqIOSMCLwHX9F1GS1FNdlntKafohZi/uhyySpBLxHaqSlCHLXZIyZLlLUoYsd0nKkB/WUcUO98EV+83Z2laeIAft99aK7LfcyvZzLj3lnemrl5Vnn8qeR+6SlCHLXZIyZLlLUoYsd0nKkOUuSRmy3CUpQ5a7JGXIcpekDFnukpQhy12SMmS5S1KGLHdJypDlLkkZstwlKUOWuyRlyHKXpAxZ7pKUIctdkjJkuUtShix3ScqQ5S5JGbLcJSlDlrskZchyl6QMWe6SlCHLXZIyZLlLUoYsd0nKkOUuSRmy3CUpQ12We0QsiYhtEdHSad4fR8TPI2Jj8fup/RtTktQT3TlybwQ+fsC8ecDDKaX3Aw8Xb0uSqkSX5Z5SWgm8fsDsS4F7i9P3Ap8qbSxJUl/09pz7aSmlV4vT/wacVqI8kqQSOK6vG0gppYhIh1seEbOB2QCjR4/u6+6q19LPlnyTc7a2lXybqj7Nr7R1TN/duLps+108Y1LZ9qXy6+2R+9aIGAFQ/L7tcCumlBallOpTSvXDhw/v5e4kST3R23JfDlxbnL4W+OfSxJEklUJ3LoX8MfAEcGZEtEbELOAO4D9FxEbgY8XbkqQq0eU595TS9MMsuqDEWSRJJeI7VCUpQ5a7JGXIcpekDFnukpQhy12SMmS5S1KGLHdJypDlLkkZstwlKUOWuyRlyHKXpAxZ7pKUoT5/WIeko9OsMn4wyIH8oJD+55G7JGXIcpekDFnukpQhy12SMmS5S1KGLHdJypDlLkkZ8jp36Rg3Z+utFdjrv1Rgn8cWj9wlKUOWuyRlyHKXpAxZ7pKUIctdkjJkuUtShix3ScqQ5S5JGbLcJSlDlrskZchyl6QMWe6SlCHLXZIy1KdRISPiRWAX8BbQnlKqL0UoSVLflGLI3/NSSq+VYDuSpBLxtIwkZaiv5Z6AFRGxJiJmlyKQJKnv+npa5iMppS0R8R7g5xHxXEppZecViqU/G2D06NF93F1lzGpc3eU6c7a29X8QSeqmPh25p5S2FL9vA34KTD7EOotSSvUppfrhw4f3ZXeSpG7qdblHxEkRMXj/NHAh0FKqYJKk3uvLaZnTgJ9GxP7tLE0p/d+SpJIk9Umvyz2ltBmYUMIskqQS8VJIScqQ5S5JGbLcJSlDpRh+oPos/WxJN+c17CqXOVtvrXSEfJW4F7rl6mXl32eRR+6SlCHLXZIyZLlLUoYsd0nKkOUuSRmy3CUpQ5a7JGXIcpekDFnukpQhy12SMmS5S1KGLHdJypDlLkkZstwlKUOWuyRlyHKXpAxZ7pKUoaPmk5hmNa7u9rp+cpJU5SrxqUjHGI/cJSlDlrskZchyl6QMWe6SlCHLXZIyZLlLUoYsd0nK0FFznbukfDS/0lbpCGVxd6f35yyeMams+/bIXZIyZLlLUoYsd0nKkOUuSRnqU7lHxMcj4vmIeCEi5pUqlCSpb3pd7hExAPgu8BfAeGB6RIwvVTBJUu/15ch9MvBCSmlzSukPwP3ApaWJJUnqi76U+0jglU63W4vzJEkV1u9vYoqI2cDs4s03I+L5/t7nksMvGga81t/776FqzATVnGveiurMVX2PVzVmgmMq14qOqSVf6PVGhgF/0tM79aXctwDv63R7VHHeu6SUFgGL+rCfkomIppRSfaVzdFaNmcBcPVWNuaoxE5irp4q5xvT0fn05LbMaeH9EnB4RfwRcBSzvw/YkSSXS6yP3lFJ7RNwI/AswAFiSUnq2ZMkkSb3Wp3PuKaWHgIdKlKUcquL00AGqMROYq6eqMVc1ZgJz9VSvckVKqdRBJEkV5vADkpSh7Mq9qyERImJGRGyPiObi1xerIVdxnc9ExPqIeDYillZDroj4u06P1e8ioq1Kco2OiEciYm1EPB0RF1dBpj+JiIeLeR6NiFH9nam43yURsS0iWg6zPCLifxRzPx0R51RBprER8URE/HtE3NTfeXqQ6z8XH6NnIuLxiJhQJbkuLeZqjoimiPhIlxtNKWXzReGF3U3AnwJ/BKwDxh+wzgzgO1WY6/3AWuDU4u33VEOuA9afQ+GF84rnonAe8obi9HjgxSrI9L+Aa4vT5wP/UKbfr48C5wAth1l+MfB/gACmAL+pgkzvASYB/x24qRyPUzdz/cdOf4N/UY7Hqpu5BvHOafRa4LmutpnbkXu1DonQnVz/BfhuSun3ACmlbVWSq7PpwI+rJFcCTi5ODwH+tQoyjQd+WZx+5BDL+0VKaSXw+hFWuRT4n6ngSeCUiBhRyUwppW0ppdXAvv7McYj9dpXr8f1/g8CTFN6/Uw253kzFZgdOovD7f0S5lXt3h0S4ovgU558i4n2HWF6JXB8APhARj0XEkxHx8SrJBRROOQCn8055VTrXAuBzEdFK4YqtOVWQaR1weXH6MmBwRAzt51zd4VAhvTOLwjOeqhARl0XEc8D/BmZ2tX5u5d4dPwPGpJRqgZ8D91Y4z37HUTg100DhCPmeiDilkoEOcBXwTymltyodpGg60JhSGkXhtMM/RESlf59vAv48ItYCf07hHdvV8nipByLiPArl/teVzrJfSumnKaWxwKeAr3e1fqX/GEqtyyERUko7Ukr/Xrz5Q+BD1ZCLwtHU8pTSvpTS/wN+R6HsK51rv6sozykZ6F6uWcA/AqSUngBqKIzBUbFMKaV/TSldnlKaCPxNcV5bP2bqrp78Ox/zIqKWQjdcmlLaUek8ByqewvnTiDji73tu5d7lkAgHnGu8BNhQDbmABykctVP8R/sAsLkKchERY4FTgSf6OU9Pcr0MXFDMN45CuW+vZKaIGNbp2cN8jjiGXVktB64pXjUzBXgjpfRqpUNVo4gYDfwE+HxK6XeVzrNfRPxZRERx+hzgBODI//GU61Xqcn1ReIr+OwpXNvxNcd5/Ay4pTn8TeJbC+dFHgLFVkiuAhcB64BngqmrIVby9ALijyv4dxwOPFf8dm4ELqyDTp4GNxXV+CJxQpsfqx8CrFF6cbKXwrOZ64PpOv1vfLeZ+BqivgkzvLc7fCbQVp0+uglw/BH5f/J1qBpqq5N/wr4u91UzhIOsjXW3Td6hKUoZyOy0jScJyl6QsWe6SlCHLXZIyZLlLUoYsd0nKkOUuSRmy3CUpQ/8fuQQDvom7/JQAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "plt.hist(transported, label=\"Transported\", alpha=.7)\n",
    "plt.hist(target, label=\"Target\", alpha=.7)\n",
    "plt.axvline(0.8, color='red')\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.17"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
