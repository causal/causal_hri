from setuptools import setup

setup(
    name='causal_hri',
    version='0.1.0',
    description='',
    url='gitlab.com/causal/causal_hri',
    packages=['causal_hri'],
    install_requires=[
        'pandas>=1.0.1',
        'numpy',
        'sklearn',
        'statsmodels',
        'scipy',
        'fcit',
        'matplotlib',
        'tableone',
        'seaborn',
        'causal-learn',
        'patsy',
        'ananke-causal',
        'jupyter'
    ]
)
