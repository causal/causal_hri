# Causal Inference for Observational Human-Robot Interaction

This project accompanies the submission "Causal Inference and Observational Human-Robot Interaction" to Transactions on 
Human-Robot Interaction. 




## Getting started
How to install:

```bash
python3 -m venv env
source env/bin/activate
pip install -e .
```

It is also required to install `graphviz`: see [here](https://graphviz.org/download/). 

```shell script

brew install graphviz # Mac

sudo apt install graphviz # Ubuntu

sudo dnf install graphviz # Fedora

```

Issues with Apple M1 Macs have been reported: see [this issue](https://github.com/pygraphviz/pygraphviz/issues/398).

```shell script
brew install graphviz
python -m pip install \
    --global-option=build_ext \
    --global-option="-I$(brew --prefix graphviz)/include/" \
    --global-option="-L$(brew --prefix graphviz)/lib/" \
    pygraphviz
```

To run the notebooks:
```bash
source env/bin/activate
python -m jupyter notebook
```

## Content of Notebooks

| Notebook  | Corresponding Sections in Paper |
|------------|---------------|
|  [Understanding Graphical Models](notebooks/00-understanding-graphical-models.ipynb) | 3.1 | 
| [Randomization](notebooks/01-randomization.ipynb) | 3.2 |
| [Adjusting for Confounding](notebooks/02-adjusting-for-confounding.ipynb) | 4.1, 4.2 |
| [Transporting Inferences](notebooks/03-transporting-inferences.ipynb) | 4.3 |
| [Measurement Error](notebooks/04-measurement-error.ipynb) | 4.4 |
| [Causal Discovery](notebooks/05-causal-discovery.ipynb) | 4.5 |
| [Trouble with Longitudinal Studies](notebooks/06-trouble-with-longitudinal-studies.ipynb) | 5.2 |
| [Inference in Longitudinal Studies](notebooks/07-inference-in-longitudinal-studies.ipynb) | 5.3 |
| [Example Study](notebooks/08-example-study.ipynb) | 6.1 |

## Authors and acknowledgment
* Jaron J. R. Lee
* Gopika Ajaykumar
* Ilya Shpitser
* Chien-Ming Huang



