import pytest
import pandas as pd
from estimators import *
from data import *
from sklearn.base import BaseEstimator
from scipy.special import expit

N = 10000
outcome = "outcome"
treatment = "treatment"
confounder = "latent_trust"
proxy_confounder = "measured_trust"
df = generate_noisy_or_dataset(
    N=N, lambda_0=0.01, p_uc=0.1, p_uy=0.1, p_ua=0.1, p_ay=0.2
)
test = get_dist(df[["measured_trust", "treatment", "outcome"]].values)
P = dict2arr(test)
M = np.array([[0.8, 0.25], [0.2, 0.75]])
I = np.linalg.inv(M)

test_data = pd.DataFrame(
    {
        "A": [0] * 8 + [1] * 8,
        "C_latent": [0, 0, 1, 1, 0, 1, 1, 0] * 2,
        "C": [0, 0, 1, 1] * 4,
        "Y": [0, 1] * 8,
    }
)

test_data_2 = pd.DataFrame(
    {"A": [0, 0, 1, 1], "C": [0, 1, 0, 0], "Y": [1, 1, 1, 1]}
)


def test_compute_pxyw():
    result = compute_pxyw(
        data=test_data,
        outcome="Y",
        treatment="A",
        proxy_confounder="C",
        treatment_value=1,
        proxy_confounder_value=1,
    )
    truth = 1 / 8
    assert result == pytest.approx(truth)


def test_compute_p_x_w():
    result = compute_p_x_w(
        data=test_data, outcome="Y", treatment="A", proxy_confounder="C"
    )

    truth = 1 / 2

    assert result[1, 0] == pytest.approx(truth)
    assert result[0, 1] == pytest.approx(truth)


def test_compute_p_w1_xy():
    result = compute_p_w1_xy(
        data=test_data_2,
        outcome="Y",
        treatment="A",
        proxy_confounder="C",
        treatment_value=1,
    )
    truth = 0.0
    assert result == pytest.approx(truth)
    result = compute_p_w1_xy(
        data=test_data_2,
        outcome="Y",
        treatment="A",
        proxy_confounder="C",
        treatment_value=0,
    )
    truth = 0.5
    assert result == pytest.approx(truth)


def test_p_generation():
    dist = get_dist(test_data[["C", "A", "Y"]].values)
    arr = dict2arr(dist)

    assert (arr == 1 / 8).all()


def test_compute_measurement_error_effect():
    np.random.seed(12312)
    print(P.dtype)

    assert P.sum() == pytest.approx(1)

    t1, t2, t3 = compute_measurement_error_effect(I, P)

    assert t1.sum() == pytest.approx(1, abs=1e-3)
    assert (t1 > 0).all()
    assert (t1 < 1).all()

    assert t2.sum() == pytest.approx(1, abs=1e-3)
    assert (t2 > 0).all()
    assert (t2 < 1).all()
    assert t3.sum() == pytest.approx(1, abs=1e-3)
    assert (t3 > 0).all()
    assert (t3 < 1).all()
    assert t3.sum(axis=0) == pytest.approx(t2, abs=1e-3)
    assert t3 == pytest.approx(t1.sum(axis=0), abs=1e-3)

    t1_truth = np.zeros((2, 2, 2))


def test_compute_combined_matrices():
    np.random.seed(12312)
    t1, t2, t3 = compute_measurement_error_effect(I, P)

    result = compute_combined_matrices(t1, t2, t3)
    assert (result > 0).all()

    assert (result < 1).all()


def test_estimate_longitudinal_effect():
    treatments = ["A_0", "A_1"]
    intermediates = ["L_1"]
    outcome = "Y"
    treatment_values = (0, 0)
    data = generate_longitudinal_fail_dataset(N=1000)
    result = estimate_longitudinal_effect(
        data, treatments, intermediates, outcome, treatment_values
    )
    treatment_values_2 = (1, 1)
    result_2 = estimate_longitudinal_effect(
        data, treatments, intermediates, outcome, treatment_values_2
    )

    assert result < 1
    assert result_2 < 1
    assert result > 0
    assert result_2 > 0


def test_estimate_longitudinal_policy():
    treatments = ["A_0", "A_1"]
    intermediates = ["L_0", "L_1"]
    outcome = "Y"
    data = generate_longitudinal_dataset(N=1000)

    class PolicyEstimatorA0(BaseEstimator):
        def __init__(self):
            pass

        def fit(self, X, y):
            return self

        def predict_proba(self, X):
            probs = []
            for i in range(X.shape[0]):
                p_1 = expit(0.3 + X[i, 0])
                p_0 = 1 - p_1
                probs.append([p_0, p_1])
            return np.array(probs)

    class PolicyEstimatorA1(BaseEstimator):
        def __init__(self):
            pass

        def fit(self, X, y):
            return self
            

        def predict_proba(self, X):
            probs = []
            for i in range(X.shape[0]):
                p_1 = expit(0.3 + X[i, 0] - 0.5 * X[i, 1] + 0.2 * X[i, 2])
                p_0 = 1 - p_1
                probs.append([p_0, p_1])
            return np.array(probs)

    A0_model = PolicyEstimatorA0()
    A1_model = PolicyEstimatorA1()

    treatment_models = {
        "A_0": A0_model,
        "A_1": A1_model,
    }

    treatment_covariates = {"A_0": ["L_0"], "A_1": ["L_0", "A_0", "L_1"]}
    result = estimate_longitudinal_policy_effect(
        data=data,
        treatments=treatments,
        intermediates=intermediates,
        outcome=outcome,
        treatment_models=treatment_models,
        treatment_covariates=treatment_covariates,
    )

    assert result < 1
    assert result > 0
    
