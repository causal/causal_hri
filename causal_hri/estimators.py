import numpy as np
from sklearn import linear_model
from scipy.special import logsumexp
import itertools
from sklearn.base import clone
from sklearn.base import BaseEstimator
from patsy import dmatrices, dmatrix
import pandas as pd


class AverageEstimator(BaseEstimator):
    def __init__(self):
        pass

    def fit(self, X, y):
        self.coefs_ = np.array([[1 - np.mean(y), np.mean(y)]])
        return self

    def predict_proba(self, X):
        return self.coefs_


def generate_X_y(data, covariates, outcome):
    if len(covariates):

        formula = "{} ~ ({}) ** {}".format(
            outcome, "+".join(covariates), len(covariates)
        )
    else:
        formula = "{} ~ 0".format(outcome)
    y, X = dmatrices(formula, data)

    return y.ravel(), X


def generate_X(data, covariates):
    if len(covariates):

        formula = "({}) ** {}".format("+".join(covariates), len(covariates))
        X = dmatrix(formula, data)
    else:
        X = None

    return X


def estimate_g_formula(
    data,
    outcome,
    treatment,
    treatment_value,
    confounders,
    model=linear_model.LinearRegression(),
):
    """

    :param data: pandas DataFrame
    :param outcome: name of the outcome or dependent variable
    :param treatment: name of the treatment or experimental condition being varied
    :param treatment_value: interventional value of the treatment
    :param confounders: list of names of confounding variables (variables which might be plausible causes of both treatment and outcome)
    :param model: a sklearn-like model.
    :return: causal estimate for the average causal effect of the treatment on the outcome
    """

    formula = "{} ~ {} ** {}".format(
        outcome,
        "+".join([treatment] + confounders),
        len([treatment] + confounders),
    )
    y, X = dmatrices(formula, data)

    model = model.fit(X=X, y=y)
    new_data = data.copy()
    new_data[treatment] = treatment_value
    _, X_tx = dmatrices(formula, new_data)

    result = model.predict(X_tx)
    estimate = np.mean(result)

    return estimate


def estimate_ipw(
    data,
    outcome,
    treatment,
    treatment_value,
    confounders,
    model=linear_model.LogisticRegression(penalty="none"),
):
    """

    :param data: pandas DataFrame
    :param outcome: name of the outcome or dependent variable
    :param treatment: name of the treatment or experimental condition being varied
    :param treatment_value: interventional value of the treatment
    :param confounders: list of names of confounding variables (variables which might be plausible causes of both treatment and outcome)
    :param model: a sklearn-like model.
    :return: causal estimate for the average causal effect of the treatment on the outcome
    """

    N = data.shape[0]

    formula = "{} ~ {} ** {}".format(
        treatment, "+".join(confounders), len(confounders)
    )
    a, X = dmatrices(formula, data)
    model = model.fit(X, a.ravel())
    result = model.predict_proba(X)[np.arange(N), a.astype(int).ravel()]
    indicator = np.array(a.astype(int).reshape(-1) == treatment_value)
    estimate = np.mean((data[outcome] * indicator) / result)

    return estimate


def estimate_g_formula_transportable(
    data_source,
    data_target,
    outcome,
    treatment,
    treatment_value,
    confounders,
    model=linear_model.LinearRegression(),
):
    """

    :param data_source: Dataframe for data from source distribution (has confounder, treatment, outcome)
    :param data_target: Dataframe for data from target distribution (only has confounders)
    :param outcome: Outcome variable name
    :param treatment: Treatment/experimental condition variable name
    :param treatment_value: Treatment/experimental condition variable value
    :param confounders: List of confounders
    :param model: sklearn-type model
    :return estimate for the average causal effect:
    """

    model = model.fit(
        X=data_source[[treatment] + confounders], y=data_source[outcome]
    )
    new_data = data_target.copy()
    new_data.loc[:, treatment] = treatment_value

    result = model.predict(new_data[[treatment] + confounders])
    estimate = np.mean(result)

    return estimate


def estimate_ipw_binary(data, outcome, treatment, confounder, treatment_value):
    """
    Estimate IPW when all variables are binary and there is only one confounder

    :param data pandas DataFram: DataFrame for data (confounder, treatment, outcome)
    :param outcome str: Name of outcome variable
    :param treatment str: Name of treatment variable
    :param confounder str: Name of confounder variable
    :param treatment_value int: Treatment value in {0, 1}
    """
    p_xyw1 = compute_pxyw(
        data, outcome, treatment, confounder, treatment_value, 1
    )
    p_xyw0 = compute_pxyw(
        data, outcome, treatment, confounder, treatment_value, 0
    )
    p_x_w = compute_p_x_w(data, outcome, treatment, confounder)

    p_x_w0 = p_x_w[treatment_value, 0]
    p_x_w1 = p_x_w[treatment_value, 1]

    return p_xyw1 / p_x_w1 + p_xyw0 / p_x_w0


def compute_pxyw(
    data,
    outcome,
    treatment,
    proxy_confounder,
    treatment_value,
    proxy_confounder_value,
):
    N = data.shape[0]
    p_xyw = (
        data[
            (data[outcome] == 1)
            & (data[treatment] == treatment_value)
            & (data[proxy_confounder] == proxy_confounder_value)
        ].shape[0]
        / N
    )

    return p_xyw


def compute_p_x_w(data, outcome, treatment, proxy_confounder):
    p_x1_w = (
        data[[treatment, proxy_confounder]]
        .groupby(proxy_confounder)
        .mean()
        .values
    )
    p_x_w = np.concatenate(
        [1 - p_x1_w.T, p_x1_w.T], axis=0
    )  # [treatment, proxy_value]
    return p_x_w


def compute_p_w1_xy(
    data, outcome, treatment, proxy_confounder, treatment_value
):

    p_w1_xy = (
        data[[proxy_confounder, treatment, outcome]]
        .groupby([treatment, outcome])
        .mean()
        .loc[(treatment_value, 1)]
        .values[0]
    )

    return p_w1_xy


def compute_I(data_error, proxy_confounder, confounder):
    prob_1 = (
        data_error[[proxy_confounder, confounder]]
        .groupby(proxy_confounder)
        .mean()
        .values.reshape(-1)
    )  # proxy_prob = np.concatenate([1 - prob_1, prob_1], axis=1)
    epsilon = prob_1[0]
    delta = 1 - prob_1[1]
    M = np.array([[1 - delta, epsilon], [delta, 1 - epsilon]])
    I = np.linalg.inv(M)
    assert delta < 0.5, delta
    assert epsilon < 0.5, epsilon

    return I


def get_dist(dist, debug=False):
    """
    Calculate the probability mass on all variable assignments
    """
    n = dist.shape[0]
    d = {}
    for assn in itertools.product(*[range(2) for _ in range(3)]):
        where = [dist[:, i] == assn[i] for i in range(len(assn))]
        where = np.all(np.stack(where, axis=0), axis=0)
        d[tuple(assn)] = np.sum(where)

    return d


def dict2arr(dictionary):
    # take all keys in dict
    l = list(dictionary.keys())
    # if result is 1D tensor
    if type(l[0]) == int:
        result = np.zeros((len(dictionary)))
    # if result is nD tensor with n > 1
    else:
        # take the maximum shape, then plus 1 to generate correct shape
        shape = [i + 1 for i in max(l)]
        result = np.zeros(shape)
    # just let the key is index and value is value of result
    for k, v in dictionary.items():
        result[k] = v

    return result / result.sum()


def compute_measurement_error_effect(I, P):
    assert (P > 0).all()
    assert (P < 1).all()
    t1 = np.zeros((2, 2, 2))
    for x in [0, 1]:
        for y in [0, 1]:
            for z in [0, 1]:
                t1[y, x, z] = np.sum([I[w, z] * P[w, x, y] for w in [0, 1]])
    t1 = np.clip(t1, a_min=0, a_max=1)
    t1 /= t1.sum()
    t2 = np.zeros(2)
    P_w = P.sum(axis=(1, 2))
    for z in [0, 1]:
        t2[z] = np.sum([I[w, z] * P_w[w] for w in [0, 1]])
    t2 = np.clip(t2, a_min=0, a_max=1)
    t2 /= t2.sum()
    t3 = np.zeros((2, 2))
    P_wx = P.sum(axis=2)
    for z in [0, 1]:
        for x in [0, 1]:
            t3[x, z] = np.sum([I[w, z] * P_wx[w, x] for w in [0, 1]])

    t3 = np.clip(t3, a_min=0, a_max=1)

    t3 /= t3.sum()

    return t1, t2, t3


def compute_combined_matrices(t1, t2, t3):
    t_final = np.zeros((2, 2))
    for y in [0, 1]:
        for x in [0, 1]:
            val = [t1[y, x, z] * t2[z] / t3[x, z] for z in [0, 1]]
            t_final[y, x] = np.sum(val)

    return t_final[1, :]


def estimate_measurement_error(
    data, data_error, treatment, confounder, proxy_confounder, outcome
):
    dict_dist = get_dist(data[[proxy_confounder, treatment, outcome]].values)

    P = dict2arr(dict_dist)

    I = compute_I(
        data_error=data_error,
        proxy_confounder=proxy_confounder,
        confounder=confounder,
    )
    t1, t2, t3 = compute_measurement_error_effect(I, P)
    t_final = compute_combined_matrices(t1, t2, t3)

    return t_final


def compute_log_odds_ratio(p_1_0, p_1_1):
    """
    Computes the log odds ratio using p(Y=1 | X=0) and p(Y=1 | X=1)

    :param p_1_0 float: p(Y=1 | X=0)
    :param p_1_1 float: p(Y=1 | X=1)
    """
    return np.log((p_1_1 / (1 - p_1_1)) / (p_1_0 / (1 - p_1_0)))


def estimate_longitudinal_effect(
    data,
    treatments,
    intermediates,
    outcome,
    treatment_values,
    model=linear_model.LogisticRegression(penalty="none"),
):
    """
    Computes longitudinal effect using the parametric g-formula. Assumes that all variables are binary.

    :param data pandas DataFrame: Dataframe containing all variables
    :param treatments list: List of names of treaments (each must be of the form "A_x" where x is an index, and A can be anything)
    :param intermediates list: List of names of intermediate variables (each must be of the form "L_x" where x is an index, and L can be anything)
    :param outcome str: Outcome variable name
    :param treatment_values tuple: Tuple of treatment values
    :param model sklearn-type: sklearn model
    """
    intermediate_values = list(
        itertools.product(range(2), repeat=len(intermediates))
    )
    intermediate_dicts = [
        dict(zip(intermediates + treatments, value + treatment_values))
        for value in intermediate_values
    ]

    model_dict = dict()
    covariate_dict = dict()
    reg_outcome = clone(model)
    y, X = generate_X_y(
        outcome=outcome, covariates=intermediates + treatments, data=data
    )
    reg_outcome = reg_outcome.fit(X=X, y=y)
    covariate_dict[outcome] = intermediates + treatments
    model_dict[outcome] = reg_outcome
    for intermediate in intermediates:

        val = intermediate.split("_")[-1]
        assigned_treatments = [x for x in treatments if x.split("_")[-1] < val]
        assigned_intermediates = [
            x for x in intermediates if x.split("_")[-1] < val
        ]
        covariates = assigned_intermediates + assigned_treatments
        y, X = generate_X_y(
            outcome=intermediate, covariates=covariates, data=data
        )
        if len(covariates):
            reg_intermediate = clone(model)
            reg_intermediate = reg_intermediate.fit(X=X, y=y)
        else:
            reg_intermediate = AverageEstimator()
            reg_intermediate = reg_intermediate.fit(X=None, y=y)

        model_dict[intermediate] = reg_intermediate
        covariate_dict[intermediate] = covariates

    total = 0
    for d in intermediate_dicts:
        part = 1
        X_dict = {var: [d[var]] for var in covariate_dict[outcome]}
        X_df = pd.DataFrame(X_dict)
        X = generate_X(covariates=covariate_dict[outcome], data=X_df)
        result = model_dict[outcome].predict_proba(X)[0, 1]
        part *= result
        for intermediate in intermediates:
            X_dict = {var: [d[var]] for var in covariate_dict[intermediate]}
            X_df = pd.DataFrame(X_dict)
            X = generate_X(covariates=covariate_dict[intermediate], data=X_df)
            result = model_dict[intermediate].predict_proba(X)[
                0, d[intermediate]
            ]
            part *= result
        total += part

    return total


def estimate_longitudinal_policy_effect(
    data,
    treatments,
    intermediates,
    outcome,
    treatment_models,
    treatment_covariates,
    model=linear_model.LogisticRegression(penalty="none"),
):
    """
    Computes longitudinal causal effect under policy functions for all treatments
    Assumes that all variables are binary.


    :param data pandas DataFrame: Data of all variables
    :param treatments list: List of names of treaments (each must be of the form "A_x" where x is an index, and A can be anything)
    :param intermediates list: List of names of intermediate variables (each must be of the form "L_x" where x is an index, and L can be anything)
    :param outcome str: Outcome variable name
    :param treatment_models dict: Mapping of treatment variable to a policy function for that model (implementing predict_proba method osklearn.base.BaseEstimator)
    :param treatment_covariates dict]: Mapping of treatment variable to covariates for policy function
    :param model sklearn-type model: Scikit-learn model
    """

    intermediate_values = list(
        itertools.product(range(2), repeat=len(intermediates + treatments))
    )
    intermediate_dicts = [
        dict(zip(intermediates + treatments, value))
        for value in intermediate_values
    ]

    model_dict = dict()
    covariate_dict = dict()

    y, X = generate_X_y(
        outcome=outcome, covariates=intermediates + treatments, data=data
    )
    reg_outcome = clone(model)
    reg_outcome = reg_outcome.fit(X=X, y=y)
    covariate_dict[outcome] = intermediates + treatments
    model_dict[outcome] = reg_outcome
    for intermediate in intermediates:
        val = intermediate.split("_")[-1]
        assigned_treatments = [x for x in treatments if x.split("_")[-1] < val]
        assigned_intermediates = [
            x for x in intermediates if x.split("_")[-1] < val
        ]
        covariates = assigned_intermediates + assigned_treatments
        y, X = generate_X_y(
            outcome=intermediate, covariates=covariates, data=data
        )
        if len(covariates):
            reg_intermediate = clone(model)
            reg_intermediate = reg_intermediate.fit(X=X, y=y)
        else:
            reg_intermediate = AverageEstimator()
            reg_intermediate = reg_intermediate.fit(X=None, y=y)

        model_dict[intermediate] = reg_intermediate
        covariate_dict[intermediate] = covariates
    for treatment in treatments:
        model_dict[treatment] = treatment_models[treatment]
        covariate_dict[treatment] = treatment_covariates[treatment]

    total = 0
    for d in intermediate_dicts:
        part = 1
        X_dict = {var: [d[var]] for var in covariate_dict[outcome]}
        X_df = pd.DataFrame(X_dict)
        X = generate_X(covariates=covariate_dict[outcome], data=X_df)
        result = model_dict[outcome].predict_proba(X)[0, 1]
        result = model_dict[outcome].predict_proba(X)[0, 1]
        part *= result
        for intermediate in intermediates:
            X_dict = {var: [d[var]] for var in covariate_dict[intermediate]}
            X_df = pd.DataFrame(X_dict)
            X = generate_X(covariates=covariate_dict[intermediate], data=X_df)
            result = model_dict[intermediate].predict_proba(X)[
                0, d[intermediate]
            ]
            part *= result
        for treatment in treatments:
            X = np.array([d[var] for var in covariate_dict[treatment]]).reshape(
                1, -1
            )
            result = model_dict[treatment].predict_proba(X)[0, d[treatment]]
            part *= result
        total += part

    return total
