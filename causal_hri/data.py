import copy

import numpy as np
import pandas as pd
import scipy as sp
from scipy.special import expit

np.random.seed(123123)


def generate_robotic_assistance_dataset(N=200, randomize=False):
    """
    Generates data inspired by the setting of Gombolay, 2018. This is a non-longitudinal dataset in which we are primarily interested in how the type of
    decision support (computer-aided versus robotic) influences subject trust, coded as a binary variable.

    A set of baseline covariates for study participants (students at a university) are collected. These include age, gender, occupation (STEM vs non-STEM majors).

    Additionally, each participant fills out a questionnaire about how they view trust. Participants are asked to give
    a rating between 1 (strongly disagree) to 5 (strongly agree) to the following statements (a selection of trust questions from Merritt et al. 2013):
    - I usually trust machines until there is a reason not to.
    - For the most part, I distrust machines.
    - In general, I would rely on a machine to assist me.

    The trust score is computed by taking an unweighted average of the responses. This score is computed at the start and at the end of the study.

    References:
    Gombomlay et al. 2018
    Merritt et al. 2013
    :param N:
    :return:
    """
    print("generating data")
    gender_p = [0.4, 0.5, 0.1]
    gender = np.random.choice(np.arange(len(gender_p)), p=gender_p, size=N)
    age = np.random.randint(low=18, high=30, size=N)
    occupation = np.random.binomial(n=1, p=0.3, size=N)

    pre_trust_q1 = np.random.randint(low=1, high=5, size=N)
    pre_trust_q2 = np.random.randint(low=1, high=5, size=N)
    pre_trust_q3 = np.random.randint(low=1, high=5, size=N)

    pre_trust = np.mean(
        np.vstack([pre_trust_q1, pre_trust_q2, pre_trust_q3]).T, axis=1
    )
    gender_params = np.array([0.5, 0.3, 0.1])

    occupation_param = 0.2
    if randomize:
        decision_support = np.random.binomial(n=1, p=0.5, size=N)
    else:
        decision_support = np.random.binomial(
            n=1,
            p=expit(
                -0.1 * (np.log(age) - np.mean(age))
                - 0.5 * (pre_trust - np.mean(pre_trust))
                - 0.2 * (occupation - np.mean(occupation))
            ),
        )

    decision_support_param = 0.2
    post_trust_q1_latent = expit(
        np.random.normal(
            loc=-1.8
            + decision_support_param * decision_support
            + 0.3 * pre_trust
            + 0.5 * np.log(age)
            + gender_params[gender]
            + occupation,
            scale=0.41,
        )
    )
    post_trust_q2_latent = expit(
        np.random.normal(
            loc=-2.8
            + decision_support_param * decision_support
            + 0.5 * pre_trust
            + 0.2 * np.log(age)
            + gender_params[gender]
            + occupation,
            scale=0.31,
        )
    )
    post_trust_q3_latent = expit(
        np.random.normal(
            loc=-1.5
            + decision_support_param * decision_support
            + 0.5 * pre_trust
            + 0.2 * np.log(age)
            + gender_params[gender]
            + occupation,
            scale=0.51,
        )
    )

    post_trust_q1 = np.digitize(post_trust_q1_latent, bins=[0.2, 0.4, 0.6, 0.8])
    post_trust_q2 = np.digitize(post_trust_q2_latent, bins=[0.2, 0.4, 0.6, 0.8])
    post_trust_q3 = np.digitize(post_trust_q3_latent, bins=[0.2, 0.4, 0.6, 0.8])
    post_trust = np.mean(
        np.vstack([post_trust_q1, post_trust_q2, post_trust_q3]).T, axis=1
    )

    df = pd.DataFrame(
        {
            "gender": gender,
            "age": age,
            "occupation": occupation,
            "pre_trust_q1": pre_trust_q1,
            "pre_trust_q2": pre_trust_q2,
            "pre_trust_q3": pre_trust_q3,
            "pre_trust": pre_trust,
            "decision_support": decision_support,
            "post_trust_q1": post_trust_q1,
            "post_trust_q2": post_trust_q2,
            "post_trust_q3": post_trust_q3,
            "post_trust": post_trust,
        }
    )

    return df


def generate_transportability_dataset(N=100, domain="US"):
    """
    Generates transportability dataset under different domains and sample sizes.

    :param N int: Number of samples
    :param domain str: Domain (US or China)
    """
    if domain == "US":
        C = np.digitize(expit(np.random.normal(loc=0, scale=1, size=N)), bins=[.2, .4, .6, .8])
    elif domain == "China":
        C = np.digitize(expit(np.random.normal(loc=-1, scale=1, size=N)), bins=[.2, .4, .6, .8])
    A = np.random.binomial(n=1, p=expit(1 + C))
    Y = np.digitize(expit(np.random.normal(loc=A + C, scale=1)), bins=[.2, .4, .6, .8])
    return pd.DataFrame({"decision_system": A, "receptiveness": C, "rating": Y})


def generate_noisy_or_dataset(
    N=100, lambda_0=0.01, p_uc=0.5, p_uy=0.5, p_ua=0.5, p_ay=0.5
):
    """
    Generates noisy OR model dataset based on
    H. Oktay, A. Atrey, and D. Jensen, “Identifying when effect restoration will improve estimates of causal effect,” in Proceedings of the 2019 SIAM international conference on data mining, 2019, pp. 190–198.

    :param N int: number of samples
    :param lambda_0 float: noise parameter of model
    :param p_uc float: correlation between latent trust and measured trust
    :param p_uy float: correlation between latent trust and outcome
    :param p_ua float: correlation between latent trust and treatment
    :param p_ay float: correlation between treatment and outcome
    """

    U = np.random.binomial(n=1, p=0.5, size=N)
    C = [
        u if np.random.uniform() < p_uc else np.random.binomial(n=1, p=0.5)
        for u in U
    ]
    A = [
        u if np.random.uniform() < p_ua else np.random.binomial(n=1, p=0.5)
        for u in U
    ]
    p_y_ua = [
        1 - (1 - lambda_0) * (1 - p_uy) ** U[i] * (1 - p_ay) ** A[i]
        for i in range(N)
    ]
    Y = np.random.binomial(n=1, p=p_y_ua)
    return pd.DataFrame(
        {"measured_trust": C, "latent_trust": U, "treatment": A, "outcome": Y}
    )


def generate_longitudinal_fail_dataset(
    N=100,
    lambda_0=0.01,
    p_uy=0.7,
    p_ul1=0.7,
    p_a0l1=0.7,
    p_a1l1=0.7,
    A0=None,
    A1=None,
):
    """
    Generates longitudinal dataset for two-phase study illustrating the spurious correlation issue.

    A0 denotes initial robot behavior, L1 denotes initial pedestrian response, A1 denotes subsequent robot behavior, Y denotes final impression, U denotes latent pedestrian factors.



    :param N int: number of samples
    :param lambda_0 float: noise parameter
    :param p_uy float: correlation between latent factor and final impression
    :param p_ull float: correlation between latent factor and initial response
    :param p_a0l1 float: correlation between initial behavior and initial response
    :param p_a1l1 float: correlation between subsequent behavior and initial response
    :param A0 int: If set, then A0 is set to value, otherwise A0 is naturally generated
    :param A1 int: If set, then A1 is set to value, otherwise A1 is naturally generated

    """
    if A0 is None:
        A0 = np.random.binomial(n=1, p=0.5, size=N)
    else:
        A0 = [A0] * N
    U = np.random.binomial(n=1, p=0.5, size=N)
    Y = [
        u if np.random.uniform() < p_uy else np.random.binomial(n=1, p=0.5)
        for u in U
    ]
    p_l1_ua0 = [
        1 - (1 - lambda_0) * (1 - p_a0l1) ** A0[i] * (1 - p_ul1) ** U[i]
        for i in range(N)
    ]
    L1 = np.random.binomial(n=1, p=p_l1_ua0)
    if A1 is None:
        A1 = [
            l1
            if np.random.uniform() < p_a1l1
            else np.random.binomial(n=1, p=0.5)
            for l1 in L1
        ]
    else:
        A1 = [A1] * N

    return pd.DataFrame({"A_0": A0, "A_1": A1, "Y": Y, "L_1": L1})


def generate_longitudinal_dataset(
    N,
    lambda_0=0.01,
    p_u0l0=0.3,
    p_a0l0=0.3,
    p_u0u1=0.3,
    p_a0l1=0.3,
    p_u0l1=0.3,
    p_u1l1=0.3,
    p_a0a1=0.3,
    p_l0a1=0.3,
    p_l1a1=0.3,
    p_a1y=0.3,
    p_u1y=0.3,
    p_l1y=0.3,
    p_a0y=0.3,
    p_l0y=0.3,
    policy_A0=None,
    policy_A1=None,
    A0=None,
    A1=None,
):
    U0 = np.random.binomial(n=1, p=0.5, size=N)
    L0 = [
        u0 if np.random.uniform() < p_u0l0 else np.random.binomial(n=1, p=0.5)
        for u0 in U0
    ]
    if A0 is not None:
        A0 = [A0] * N
    elif policy_A0 is not None:
        p_a0 = policy_A0.predict_proba(np.array([L0]).T)[:, 1]
        A0 = np.random.binomial(n=1, p=p_a0)
    else:
        A0 = [
            l0
            if np.random.uniform() < p_a0l0
            else np.random.binomial(n=1, p=0.5)
            for l0 in L0
        ]

    U1 = [
        u0 if np.random.uniform() < p_u0u1 else np.random.binomial(n=1, p=0.5)
        for u0 in U0
    ]

    p_l1 = [
        1
        - (1 - lambda_0)
        * (1 - p_a0l1) ** A0[i]
        * (1 - p_u0l1) ** U0[i]
        * (1 - p_u1l1) ** U1[i]
        for i in range(N)
    ]

    L1 = np.random.binomial(n=1, p=p_l1)
    if A1 is not None:
        A1 = [A1] * N
    elif policy_A1 is not None:
        p_a1 = policy_A1.predict_proba(np.array([L0, L1, A0]).T)[:, 1]
        A1 = np.random.binomial(n=1, p=p_a1)
    else:
        p_a1 = [
            1
            - (1 - lambda_0)
            * (1 - p_a0a1) ** A0[i]
            * (1 - p_l0a1) ** L0[i]
            * (1 - p_l1a1) ** L1[i]
            for i in range(N)
        ]

        A1 = np.random.binomial(n=1, p=p_a1)

    p_y = [
        1
        - (1 - lambda_0)
        * (1 - p_a1y) ** A1[i]
        * (1 - p_u1y) ** U1[i]
        * (1 - p_l1y) ** L1[i]
        * (1 - p_a0y) ** A0[i]
        * (1 - p_l0y) ** L0[i]
        for i in range(N)
    ]

    Y = np.random.binomial(n=1, p=p_y)

    return pd.DataFrame(
        {
            "A_0": A0,
            "A_1": A1,
            "U_0": U0,
            "U_1": U1,
            "L_0": L0,
            "L_1": L1,
            "Y": Y,
        }
    )


def generate_robot_advertising_dataset(N=200):
    """
    Generates data for the robot advertising example.

    :param N [TODO:type]: [TODO:description]
    """
    C = np.random.binomial(n=1, p=0.5, size=N)
    A = np.random.binomial(n=1, p=expit(0.3 + 0.2 * C))
    M = np.random.binomial(
        n=1, p=expit(-0.1 + 0.25 * C + 0.1 * A + 0.12 * A * C)
    )
    Y = np.random.binomial(
        n=1, p=expit(-0.2 + 0.2 * C - 0.3 * A + 0.1 * M + 0.1 * A * C * M)
    )
    G = np.random.binomial(
        n=1, p=expit(-0.2 + 0.1 * C - 0.2 * A + 0.4 * M + 0.5 * Y)
    )

    df = pd.DataFrame({"ses": C, "ad_type": A, "recep": M, "rev": Y, "qual": G})

    return df
